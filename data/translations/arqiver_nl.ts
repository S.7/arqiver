<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl_NL">
<context>
    <name>Arqiver::AboutDialog</name>
    <message>
        <location filename="../../about.ui" line="128"/>
        <source>License</source>
        <translation>Licentie</translation>
    </message>
</context>
<context>
    <name>Arqiver::Backend</name>
    <message>
        <location filename="../../backends.cpp" line="631"/>
        <location filename="../../backends.cpp" line="664"/>
        <source>This file is a link but its target does not exist.</source>
        <translation>Dit bestand is een snelkoppeling, maar het doel bestaat niet.</translation>
    </message>
    <message>
        <location filename="../../backends.cpp" line="1047"/>
        <location filename="../../backends.cpp" line="1078"/>
        <location filename="../../backends.cpp" line="1121"/>
        <source>Could not read archive</source>
        <translation>Het archief kan niet worden uitgelezen</translation>
    </message>
    <message>
        <location filename="../../backends.cpp" line="1081"/>
        <location filename="../../backends.cpp" line="1126"/>
        <source>Archive Loaded</source>
        <translation>Het archief is geladen</translation>
    </message>
    <message>
        <location filename="../../backends.cpp" line="1088"/>
        <location filename="../../backends.cpp" line="1174"/>
        <source>Modification Finished</source>
        <translation>De bewerking is voltooid</translation>
    </message>
    <message>
        <location filename="../../backends.cpp" line="1109"/>
        <location filename="../../backends.cpp" line="1143"/>
        <source>Extraction Finished</source>
        <translation>Het uitpakken is voltooid</translation>
    </message>
    <message>
        <location filename="../../backends.cpp" line="1112"/>
        <location filename="../../backends.cpp" line="1162"/>
        <source>Extraction Failed</source>
        <translation>Het uitpakken is mislukt</translation>
    </message>
    <message>
        <location filename="../../backends.cpp" line="1271"/>
        <source>%1 is missing from your system.
Please install it for this kind of archive!</source>
        <translation>%1 is niet geïnstalleerd.
Installeer het om dit soort archieven te kunnen beheren!</translation>
    </message>
</context>
<context>
    <name>Arqiver::LineEdit</name>
    <message>
        <location filename="../../lineedit.h" line="33"/>
        <source>You could type inside the main view.
Clear text with the Escape key.</source>
        <translation>Je kunt typen in de weergave en
tekst wissen met de escapetoets.</translation>
    </message>
</context>
<context>
    <name>Arqiver::PrefDialog</name>
    <message>
        <location filename="../../pref.ui" line="14"/>
        <source>Preferences</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../../pref.ui" line="45"/>
        <source>Start with this size:</source>
        <translation>Standaardafmetingen:</translation>
    </message>
    <message>
        <location filename="../../pref.ui" line="94"/>
        <source>Show prompt on removing items</source>
        <translation>Om bevestiging vragen bij verwijderen van meerdere items</translation>
    </message>
    <message>
        <location filename="../../pref.ui" line="101"/>
        <source>Fit first column into available width</source>
        <translation>Eerste kolom aanpassen aan beschikbare breedte</translation>
    </message>
    <message>
        <location filename="../../pref.ui" line="108"/>
        <source>If this is unchecked, only the top level directories will be expanded.</source>
        <translation>Schakel uit om alleen mappen op het bovenste niveau uit te klappen.</translation>
    </message>
    <message>
        <location filename="../../pref.ui" line="111"/>
        <source>Expand all directories when opening archives</source>
        <translation>Alle mappen uitklappen na openen van een archief</translation>
    </message>
    <message>
        <location filename="../../pref.ui" line="118"/>
        <location filename="../../pref.ui" line="131"/>
        <source>Leave empty for the default.

Warning: Arqiver will not work if the
binary does not belong to libarchive.</source>
        <translation>Laat leeg om de standaardwaarde te gebruiken.

Waarschuwing: Arqiver werkt niet als het uitvoerbare
bestand niet bij libarchive hoor.</translation>
    </message>
    <message>
        <location filename="../../pref.ui" line="124"/>
        <source>libarchive binary:</source>
        <translation>libarchive - uitvoerbaar bestand:</translation>
    </message>
    <message>
        <location filename="../../pref.ui" line="38"/>
        <source>Remember window &amp;size on closing</source>
        <translation>Ven&amp;sterafmetingen bewaren na afsluiten</translation>
    </message>
    <message>
        <location filename="../../pref.ui" line="83"/>
        <source>Size of view icons:</source>
        <translation>Grootte van weergavepictogrammen:</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="56"/>
        <source>px</source>
        <translation>px</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="226"/>
        <location filename="../../pref.cpp" line="251"/>
        <source>Application restart is needed for changes to take effect.</source>
        <translation>Herstart Arqiver om de wijzigingen toe te passen.</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="240"/>
        <source>Select libarchive binary</source>
        <translation>Kies het libarchive-bestand</translation>
    </message>
</context>
<context>
    <name>Arqiver::mainWin</name>
    <message>
        <location filename="../../mainWin.ui" line="53"/>
        <source>Archive:</source>
        <translation>Archief:</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="106"/>
        <source>Filter Files...</source>
        <translation>Bestanden filteren…</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="129"/>
        <source>&amp;File</source>
        <translation>&amp;Bestand</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="140"/>
        <source>&amp;Edit</source>
        <translation>B&amp;ewerken</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="153"/>
        <source>&amp;View</source>
        <translation>&amp;Beeld</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="162"/>
        <source>&amp;Help</source>
        <translation>&amp;Hulp</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="198"/>
        <source>&amp;Open Archive</source>
        <translation>Archief &amp;openen</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="201"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="206"/>
        <source>&amp;New Archive</source>
        <translation>&amp;Nieuw</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="209"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="214"/>
        <source>&amp;Quit</source>
        <translation>&amp;Afsluiten</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="217"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="222"/>
        <source>Add File(s)</source>
        <translation>Bestand(en) toevoegen</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="227"/>
        <source>Remove File(s)</source>
        <translation>Bestand(en) verwijderen</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="232"/>
        <source>Extract All</source>
        <translation>Alles uitpakken</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="235"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="240"/>
        <source>Add Directory</source>
        <translation>Map toevoegen</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="245"/>
        <source>Extract Selection</source>
        <translation>Selectie uitpakken</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="250"/>
        <source>Set Password</source>
        <translation>Wachtwoord instellen</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="255"/>
        <source>E&amp;xpand</source>
        <translation>U&amp;itklappen</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="258"/>
        <source>Ctrl+Shift+Down</source>
        <translation>Ctrl+Shift+pijltje omlaag</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="263"/>
        <source>Collap&amp;se</source>
        <translation>In&amp;klappen</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="266"/>
        <source>Ctrl+Shift+Up</source>
        <translation>Ctrl+Shift+pijltje omhoog</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="271"/>
        <source>&amp;About</source>
        <translation>&amp;Over</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="276"/>
        <source>&amp;Copy Current Path</source>
        <translation>Huidige lo&amp;catie kopiëren</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="281"/>
        <source>&amp;Preferences</source>
        <translation>&amp;Instellingen</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="286"/>
        <source>Stop &amp;Process</source>
        <translation>&amp;Proces afbreken</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="289"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="89"/>
        <source>Root</source>
        <translation>Hoofdmap</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="148"/>
        <source>Error</source>
        <translation>Foutmelding</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="205"/>
        <source>File</source>
        <translation>Bestand</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="205"/>
        <source>MimeType</source>
        <translation>Mimetype</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="205"/>
        <source>Size</source>
        <translation>Grootte</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="416"/>
        <location filename="../../mainWin.cpp" line="686"/>
        <location filename="../../mainWin.cpp" line="727"/>
        <source>Opening Archive...</source>
        <translation>Bezig met openen van archief…</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="490"/>
        <source>All Types %1</source>
        <translation>Alle soorten %1</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="500"/>
        <location filename="../../mainWin.cpp" line="524"/>
        <source>Uncompressed Archive (*.tar)</source>
        <translation>Archief zonder compressie (*.tar)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="501"/>
        <location filename="../../mainWin.cpp" line="525"/>
        <source>GZip Compressed Archive (*.tar.gz *.tgz)</source>
        <translation>GZip-archief met compressie (*.tar.gz *.tgz)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="503"/>
        <location filename="../../mainWin.cpp" line="527"/>
        <source>BZip2 Compressed Archive (*.tar.bz2 *.tbz2 *.tbz)</source>
        <translation>BZip2-archief met compressie (*.tar.bz2 *.tbz2 *.tbz)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="504"/>
        <location filename="../../mainWin.cpp" line="533"/>
        <source>LMZA Compressed Archive (*.tar.lzma *.tlz)</source>
        <translation>LMZA-archief met compressie (*.tar.lzma *.tlz)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="505"/>
        <location filename="../../mainWin.cpp" line="530"/>
        <source>XZ Compressed Archive (*.tar.xz *.txz)</source>
        <translation>XZ-archief met compressie (*.tar.xz *.txz)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="506"/>
        <location filename="../../mainWin.cpp" line="534"/>
        <source>Zstandard Compressed Archive (*.tar.zst *.tzst)</source>
        <translation>Zstandard-archief met compressie (*.tar.zst *.tzst)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="507"/>
        <location filename="../../mainWin.cpp" line="536"/>
        <source>CPIO Archive (*.cpio)</source>
        <translation>CPIO-archief (*.cpio)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="509"/>
        <location filename="../../mainWin.cpp" line="538"/>
        <source>AR Archive (*.ar)</source>
        <translation>AR-archief (*.ar)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="511"/>
        <location filename="../../mainWin.cpp" line="540"/>
        <source>Zip Archive (*.zip)</source>
        <translation>Zip-archief (*.zip)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="512"/>
        <location filename="../../mainWin.cpp" line="542"/>
        <source>Gzip Archive (*.gz)</source>
        <translation>GZip-archief (*.gz)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="513"/>
        <location filename="../../mainWin.cpp" line="541"/>
        <source>7-Zip Archive (*.7z)</source>
        <translation>7-Zip-archief (*.7z)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="528"/>
        <source>READ-ONLY: BZip2 Archive (*.bz2)</source>
        <translation>ALLEEN-LEZEN: BZip2-archief (*.bz2)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="529"/>
        <source>READ-ONLY: BZip2 Compressed PDF Document (*.pdf.bz2)</source>
        <translation>ALLEEN-LEZEN: BZip2-PDF-document met compressie (*.pdf.bz2)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="531"/>
        <source>READ-ONLY: XZ archive (*.xz)</source>
        <translation>ALLEEN-LEZEN: XZ-archief (*.xz)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="532"/>
        <source>READ-ONLY: XZ Compressed PDF Document (*.pdf.xz)</source>
        <translation>ALLEEN-LEZEN: XZ-PDF-document met compressie (*.pdf.xz)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="535"/>
        <source>READ-ONLY: Zstandard archive (*.zst)</source>
        <translation>ALLEEN-LEZEN: Zstandard-archief (*.zst)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="543"/>
        <source>Gzip Compressed PDF Document (*.pdf.gz)</source>
        <translation>GZip-PDF-document met compressie (*.pdf.gz)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="544"/>
        <source>READ-ONLY: Compressed SVG Image (*.svgz)</source>
        <translation>ALLEEN-LEZEN: SVG-afbeelding met compressie (*.svgz)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="545"/>
        <location filename="../../mainWin.cpp" line="546"/>
        <source>READ-ONLY: ISO Image (*.iso *.img)</source>
        <translation>ALLEEN-LEZEN: ISO-schijfkopie (*.iso *.img)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="547"/>
        <source>READ-ONLY: XAR Archive (*.xar)</source>
        <translation>ALLEEN-LEZEN: XAR-archief (*.xar)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="548"/>
        <source>READ-ONLY: Java Archive (*.jar)</source>
        <translation>ALLEEN-LEZEN: Java-archief (*.jar)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="549"/>
        <source>READ-ONLY: Debian Package (*.deb)</source>
        <translation>ALLEEN-LEZEN: Debian-pakket (*.deb)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="550"/>
        <location filename="../../mainWin.cpp" line="551"/>
        <source>READ-ONLY: RedHat Package (*.rpm)</source>
        <translation>ALLEEN-LEZEN: RedHat-pakket (*.rpm)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="552"/>
        <source>READ-ONLY: MS Windows Executable (*.exe *.com)</source>
        <translation>ALLEEN-LEZEN: MS Windows uitvoerbaar bestand (*.exe *.com)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="553"/>
        <source>READ-ONLY: MS Windows Installer Package (*.msi)</source>
        <translation>ALLEEN-LEZEN: MS Windows-installatiepakket (*.msi)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="554"/>
        <source>READ-ONLY: MS Windows Cabinet Archive (*.cab)</source>
        <translation>ALLEEN-LEZEN: MS Windows-cabinetarchief (*.cab)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="555"/>
        <source>READ-ONLY: ACE archive (*.ace)</source>
        <translation>ALLEEN-LEZEN: ACE-archief (*.ace)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="556"/>
        <source>READ-ONLY: Android Package (*.apk)</source>
        <translation>ALLEEN-LEZEN: Android-pakket (*.apk)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="557"/>
        <source>READ-ONLY: RAR Archive (*.rar)</source>
        <translation>ALLEEN-LEZEN: RAR-archief (*.rar)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="558"/>
        <source>READ-ONLY: AppImage application bundle (*.appimage)</source>
        <translation>ALLEEN-LEZEN: AppImage-programmabundel (*.appimage)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="567"/>
        <source>All Known Types %1</source>
        <translation>Alle bekende soorten %1</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="572"/>
        <source>Show All Files (*)</source>
        <translation>Alle bestanden tonen (*)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="622"/>
        <source>Create Archive</source>
        <translation>Archief samenstellen</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="648"/>
        <location filename="../../mainWin.cpp" line="836"/>
        <location filename="../../mainWin.cpp" line="1110"/>
        <source>Question</source>
        <translation>Vraag</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="649"/>
        <source>The following archive already exists:</source>
        <translation>Het volgende archief bestaat al:</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="651"/>
        <source>Do you want to replace it?
</source>
        <translation>Wil je dit vervangen?
</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="678"/>
        <source>Open Archive</source>
        <translation>Archief openen</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="742"/>
        <location filename="../../mainWin.cpp" line="749"/>
        <location filename="../../mainWin.cpp" line="785"/>
        <source>Add to Archive</source>
        <translation>Toevoegen aan archief</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="780"/>
        <location filename="../../mainWin.cpp" line="811"/>
        <location filename="../../mainWin.cpp" line="1065"/>
        <location filename="../../mainWin.cpp" line="1071"/>
        <source>Adding Items...</source>
        <translation>Bezig met toevoegen van items…</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="837"/>
        <source>Do you want to remove the selected item(s)?
</source>
        <translation>Wil je de geselecteerde items verwijderen?
</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="854"/>
        <source>Removing Items...</source>
        <translation>Bezig met verwijderen van items…</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1028"/>
        <location filename="../../mainWin.cpp" line="1043"/>
        <location filename="../../mainWin.cpp" line="1123"/>
        <location filename="../../mainWin.cpp" line="1137"/>
        <source>Extracting...</source>
        <translation>Bezig met uitpakken…</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="894"/>
        <source>Copy Archive Path</source>
        <translation>Archieflocatie kopiëren</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="899"/>
        <source>Open Containing Folder</source>
        <translation>Bijbehorende map openen</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="917"/>
        <source>View Current Item</source>
        <translation>Huidig item bekijken</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="932"/>
        <location filename="../../mainWin.cpp" line="940"/>
        <source>Enter Password</source>
        <translation>Voer het wachtwoord in</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="944"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="945"/>
        <source>OK</source>
        <translation>Oké</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="949"/>
        <source>Encrypt the file list</source>
        <translation>Bestandslijst versleutelen</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="950"/>
        <source>This will take effect after files/folders are added.</source>
        <translation>Dit wordt toegepast ná het toevoegen van bestanden/mappen.</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1021"/>
        <location filename="../../mainWin.cpp" line="1097"/>
        <source>Extract Into Directory</source>
        <translation>Uitpakken naar map</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1111"/>
        <source>Some files will be overwritten.
Do you want to continue?
</source>
        <translation>Enkele bestanden worden overschreven.
Weet je zeker dat je wilt doorgaan?
</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1259"/>
        <source>Link To: %1</source>
        <translation>Link naar %1</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1606"/>
        <source>A simple Qt archive manager</source>
        <translation>Een eenvoudige Qt-archiefbeheerder</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1607"/>
        <source>based on libarchive, gzip and 7z</source>
        <translation>gebaseerd op libarchive, gzip en 7z</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1608"/>
        <source>Author</source>
        <translation>Maker</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1609"/>
        <source>aka.</source>
        <translation>ook wel bekend als</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1610"/>
        <location filename="../../mainWin.cpp" line="1611"/>
        <source>About Arqiver</source>
        <translation>Over Arqiver</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1610"/>
        <source>Translators</source>
        <translation>Vertalers</translation>
    </message>
</context>
</TS>
