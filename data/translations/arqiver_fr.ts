<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>Arqiver::AboutDialog</name>
    <message>
        <location filename="../../about.ui" line="128"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
</context>
<context>
    <name>Arqiver::Backend</name>
    <message>
        <location filename="../../backends.cpp" line="631"/>
        <location filename="../../backends.cpp" line="664"/>
        <source>This file is a link but its target does not exist.</source>
        <translation>Ce fichier est un lien mais sa cible n&apos;existe pas.</translation>
    </message>
    <message>
        <location filename="../../backends.cpp" line="1047"/>
        <location filename="../../backends.cpp" line="1078"/>
        <location filename="../../backends.cpp" line="1121"/>
        <source>Could not read archive</source>
        <translation>Impossible de lire l&apos;archive</translation>
    </message>
    <message>
        <location filename="../../backends.cpp" line="1081"/>
        <location filename="../../backends.cpp" line="1126"/>
        <source>Archive Loaded</source>
        <translation>Archive chargée</translation>
    </message>
    <message>
        <location filename="../../backends.cpp" line="1088"/>
        <location filename="../../backends.cpp" line="1174"/>
        <source>Modification Finished</source>
        <translation>Modification terminée</translation>
    </message>
    <message>
        <location filename="../../backends.cpp" line="1109"/>
        <location filename="../../backends.cpp" line="1143"/>
        <source>Extraction Finished</source>
        <translation>Extraction terminée</translation>
    </message>
    <message>
        <location filename="../../backends.cpp" line="1112"/>
        <location filename="../../backends.cpp" line="1162"/>
        <source>Extraction Failed</source>
        <translation>L&apos;extraction a échoué</translation>
    </message>
    <message>
        <location filename="../../backends.cpp" line="1271"/>
        <source>%1 is missing from your system.
Please install it for this kind of archive!</source>
        <translation>%1 est manquant dans votre système.
Veuillez l&apos;installer pour ce genre d&apos;archive&#xa0;!</translation>
    </message>
</context>
<context>
    <name>Arqiver::LineEdit</name>
    <message>
        <location filename="../../lineedit.h" line="33"/>
        <source>You could type inside the main view.
Clear text with the Escape key.</source>
        <translation>Vous pouvez taper dans la vue principale.
Effacez le texte avec la touche Echap.</translation>
    </message>
</context>
<context>
    <name>Arqiver::PrefDialog</name>
    <message>
        <location filename="../../pref.ui" line="14"/>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="../../pref.ui" line="45"/>
        <source>Start with this size:</source>
        <translation>Commencer par cette taille&#xa0;:</translation>
    </message>
    <message>
        <location filename="../../pref.ui" line="94"/>
        <source>Show prompt on removing items</source>
        <translation>Afficher l&apos;invite lors de la suppression d&apos;éléments</translation>
    </message>
    <message>
        <location filename="../../pref.ui" line="101"/>
        <source>Fit first column into available width</source>
        <translation>Adapter la première colonne à la largeur disponible</translation>
    </message>
    <message>
        <location filename="../../pref.ui" line="108"/>
        <source>If this is unchecked, only the top level directories will be expanded.</source>
        <translation>Si cette option n&apos;est pas cochée, seuls les répertoires de niveau supérieur seront développés.</translation>
    </message>
    <message>
        <location filename="../../pref.ui" line="111"/>
        <source>Expand all directories when opening archives</source>
        <translation>Développer tous les répertoires lors de l&apos;ouverture des archives</translation>
    </message>
    <message>
        <location filename="../../pref.ui" line="118"/>
        <location filename="../../pref.ui" line="131"/>
        <source>Leave empty for the default.

Warning: Arqiver will not work if the
binary does not belong to libarchive.</source>
        <translation>Laissez vide pour la valeur par défaut.

Attention&#xa0;: Arqiver ne fonctionnera pas si le
binaire n&apos;appartient pas à libarchive.</translation>
    </message>
    <message>
        <location filename="../../pref.ui" line="124"/>
        <source>libarchive binary:</source>
        <translation>binaire libarchive&#xa0;:</translation>
    </message>
    <message>
        <location filename="../../pref.ui" line="38"/>
        <source>Remember window &amp;size on closing</source>
        <translation>Se souvenir de la &amp;taille de la fenêtre à la fermeture</translation>
    </message>
    <message>
        <location filename="../../pref.ui" line="83"/>
        <source>Size of view icons:</source>
        <translation>Taille des icônes de vue&#xa0;:</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="56"/>
        <source>px</source>
        <translation>px</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="226"/>
        <location filename="../../pref.cpp" line="251"/>
        <source>Application restart is needed for changes to take effect.</source>
        <translation>Le redémarrage de l&apos;application est nécessaire pour que les modifications prennent effet.</translation>
    </message>
    <message>
        <location filename="../../pref.cpp" line="240"/>
        <source>Select libarchive binary</source>
        <translation>Sélectionner le binaire libarchive</translation>
    </message>
</context>
<context>
    <name>Arqiver::mainWin</name>
    <message>
        <location filename="../../mainWin.ui" line="53"/>
        <source>Archive:</source>
        <translation>Archive&#xa0;:</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="106"/>
        <source>Filter Files...</source>
        <translation>Filtrer les fichiers...</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="129"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="140"/>
        <source>&amp;Edit</source>
        <translation>&amp;Edition</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="153"/>
        <source>&amp;View</source>
        <translation>&amp;Vue</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="162"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="198"/>
        <source>&amp;Open Archive</source>
        <translation>&amp;Ouvrir une Archive</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="201"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="206"/>
        <source>&amp;New Archive</source>
        <translation>&amp;Nouvelle Archive</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="209"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="214"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="217"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="222"/>
        <source>Add File(s)</source>
        <translation>Ajouter des fichiers</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="227"/>
        <source>Remove File(s)</source>
        <translation>Supprimer le(s) fichier(s)</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="232"/>
        <source>Extract All</source>
        <translation>Extraire tout</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="235"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="240"/>
        <source>Add Directory</source>
        <translation>Ajouter un répertoire</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="245"/>
        <source>Extract Selection</source>
        <translation>Extraire la sélection</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="250"/>
        <source>Set Password</source>
        <translation>Définir le mot de passe</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="255"/>
        <source>E&amp;xpand</source>
        <translation>Dé&amp;velopper</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="258"/>
        <source>Ctrl+Shift+Down</source>
        <translation>Ctrl+Maj+Bas</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="263"/>
        <source>Collap&amp;se</source>
        <translation type="unfinished">Effond&amp;rer</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="266"/>
        <source>Ctrl+Shift+Up</source>
        <translation>Ctrl+Maj+Haut</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="271"/>
        <source>&amp;About</source>
        <translation>&amp;A propos</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="276"/>
        <source>&amp;Copy Current Path</source>
        <translation>&amp;Copier le chemin actuel</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="281"/>
        <source>&amp;Preferences</source>
        <translation>&amp;Préférences</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="286"/>
        <source>Stop &amp;Process</source>
        <translation>Arrêter &amp;Traiter</translation>
    </message>
    <message>
        <location filename="../../mainWin.ui" line="289"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="89"/>
        <source>Root</source>
        <translation>Racine</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="148"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="205"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="205"/>
        <source>MimeType</source>
        <translation type="unfinished">TypeMime</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="205"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="416"/>
        <location filename="../../mainWin.cpp" line="686"/>
        <location filename="../../mainWin.cpp" line="727"/>
        <source>Opening Archive...</source>
        <translation>Ouverture de l&apos;archive...</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="490"/>
        <source>All Types %1</source>
        <translation>Tous types %1</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="500"/>
        <location filename="../../mainWin.cpp" line="524"/>
        <source>Uncompressed Archive (*.tar)</source>
        <translation>Archive non compressée (*.tar)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="501"/>
        <location filename="../../mainWin.cpp" line="525"/>
        <source>GZip Compressed Archive (*.tar.gz *.tgz)</source>
        <translation>Archive compressée GZip (*.tar.gz *.tgz)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="503"/>
        <location filename="../../mainWin.cpp" line="527"/>
        <source>BZip2 Compressed Archive (*.tar.bz2 *.tbz2 *.tbz)</source>
        <translation>Archive compressée BZip2 (*.tar.bz2 *.tbz2 *.tbz)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="504"/>
        <location filename="../../mainWin.cpp" line="533"/>
        <source>LMZA Compressed Archive (*.tar.lzma *.tlz)</source>
        <translation>Archive compressée LMZA (*.tar.lzma *.tlz)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="505"/>
        <location filename="../../mainWin.cpp" line="530"/>
        <source>XZ Compressed Archive (*.tar.xz *.txz)</source>
        <translation>Archive compressée XZ (*.tar.xz *.txz)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="506"/>
        <location filename="../../mainWin.cpp" line="534"/>
        <source>Zstandard Compressed Archive (*.tar.zst *.tzst)</source>
        <translation>Archive compressée Zstandard (*.tar.zst *.tzst)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="507"/>
        <location filename="../../mainWin.cpp" line="536"/>
        <source>CPIO Archive (*.cpio)</source>
        <translation>Archive CPIO (*.cpio)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="509"/>
        <location filename="../../mainWin.cpp" line="538"/>
        <source>AR Archive (*.ar)</source>
        <translation>Archive AR (*.ar)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="511"/>
        <location filename="../../mainWin.cpp" line="540"/>
        <source>Zip Archive (*.zip)</source>
        <translation>Archive Zip (*.zip)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="512"/>
        <location filename="../../mainWin.cpp" line="542"/>
        <source>Gzip Archive (*.gz)</source>
        <translation>Archives Gzip (*.gz)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="513"/>
        <location filename="../../mainWin.cpp" line="541"/>
        <source>7-Zip Archive (*.7z)</source>
        <translation>Archive 7-Zip (*.7z)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="528"/>
        <source>READ-ONLY: BZip2 Archive (*.bz2)</source>
        <translation>LECTURE SEULE&#xa0;: Archive BZip2 (*.bz2)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="529"/>
        <source>READ-ONLY: BZip2 Compressed PDF Document (*.pdf.bz2)</source>
        <translation>LECTURE SEULE&#xa0;: Document PDF compressé BZip2 (*.pdf.bz2)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="531"/>
        <source>READ-ONLY: XZ archive (*.xz)</source>
        <translation>LECTURE SEULE&#xa0;: archive XZ (*.xz)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="532"/>
        <source>READ-ONLY: XZ Compressed PDF Document (*.pdf.xz)</source>
        <translation>LECTURE SEULE&#xa0;: Document PDF compressé XZ (*.pdf.xz)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="535"/>
        <source>READ-ONLY: Zstandard archive (*.zst)</source>
        <translation>LECTURE SEULE&#xa0;: archive Zstandard (*.zst)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="543"/>
        <source>Gzip Compressed PDF Document (*.pdf.gz)</source>
        <translation>Document PDF compressé Gzip (*.pdf.gz)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="544"/>
        <source>READ-ONLY: Compressed SVG Image (*.svgz)</source>
        <translation>LECTURE SEULE&#xa0;: Image SVG compressée (*.svgz)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="545"/>
        <location filename="../../mainWin.cpp" line="546"/>
        <source>READ-ONLY: ISO Image (*.iso *.img)</source>
        <translation>LECTURE SEULE&#xa0;: Image ISO (*.iso *.img)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="547"/>
        <source>READ-ONLY: XAR Archive (*.xar)</source>
        <translation>LECTURE SEULE&#xa0;: Archive XAR (*.xar)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="548"/>
        <source>READ-ONLY: Java Archive (*.jar)</source>
        <translation>LECTURE SEULE&#xa0;: Archive Java (*.jar)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="549"/>
        <source>READ-ONLY: Debian Package (*.deb)</source>
        <translation>LECTURE SEULE&#xa0;: Paquet Debian (*.deb)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="550"/>
        <location filename="../../mainWin.cpp" line="551"/>
        <source>READ-ONLY: RedHat Package (*.rpm)</source>
        <translation>LECTURE SEULE&#xa0;: Paquet RedHat (*.rpm)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="552"/>
        <source>READ-ONLY: MS Windows Executable (*.exe *.com)</source>
        <translation>LECTURE SEULE&#xa0;: exécutable MS Windows (*.exe *.com)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="553"/>
        <source>READ-ONLY: MS Windows Installer Package (*.msi)</source>
        <translation>LECTURE SEULE : Package d&apos;installation MS Windows (*.msi)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="554"/>
        <source>READ-ONLY: MS Windows Cabinet Archive (*.cab)</source>
        <translation>LECTURE SEULE&#xa0;: Archive MS Windows Cabinet (*.cab)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="555"/>
        <source>READ-ONLY: ACE archive (*.ace)</source>
        <translation>LECTURE SEULE&#xa0;: archive ACE (*.ace)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="556"/>
        <source>READ-ONLY: Android Package (*.apk)</source>
        <translation>LECTURE SEULE&#xa0;: Package Android (*.apk)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="557"/>
        <source>READ-ONLY: RAR Archive (*.rar)</source>
        <translation>LECTURE SEULE : Archive RAR (*.rar)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="558"/>
        <source>READ-ONLY: AppImage application bundle (*.appimage)</source>
        <translation>LECTURE SEULE&#xa0;: Ensemble d&apos;applications AppImage (*.appimage)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="567"/>
        <source>All Known Types %1</source>
        <translation>Tous types connus %1</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="572"/>
        <source>Show All Files (*)</source>
        <translation>Afficher tous les fichiers (*)</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="622"/>
        <source>Create Archive</source>
        <translation>Créer une archive</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="648"/>
        <location filename="../../mainWin.cpp" line="836"/>
        <location filename="../../mainWin.cpp" line="1110"/>
        <source>Question</source>
        <translation>Question</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="649"/>
        <source>The following archive already exists:</source>
        <translation>L&apos;archive suivante existe déjà&#xa0;:</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="651"/>
        <source>Do you want to replace it?
</source>
        <translation>Voulez-vous le remplacer&#xa0;?
</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="678"/>
        <source>Open Archive</source>
        <translation>Ouvrir l&apos;archive</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="742"/>
        <location filename="../../mainWin.cpp" line="749"/>
        <location filename="../../mainWin.cpp" line="785"/>
        <source>Add to Archive</source>
        <translation>Ajouter à l&apos;archive</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="780"/>
        <location filename="../../mainWin.cpp" line="811"/>
        <location filename="../../mainWin.cpp" line="1065"/>
        <location filename="../../mainWin.cpp" line="1071"/>
        <source>Adding Items...</source>
        <translation>Ajout d&apos;éléments...</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="837"/>
        <source>Do you want to remove the selected item(s)?
</source>
        <translation>Voulez-vous supprimer le ou les éléments sélectionnés&#xa0;?
</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="854"/>
        <source>Removing Items...</source>
        <translation>Suppression d&apos;éléments...</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1028"/>
        <location filename="../../mainWin.cpp" line="1043"/>
        <location filename="../../mainWin.cpp" line="1123"/>
        <location filename="../../mainWin.cpp" line="1137"/>
        <source>Extracting...</source>
        <translation>Extraction...</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="894"/>
        <source>Copy Archive Path</source>
        <translation>Copier le chemin de l&apos;archive</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="899"/>
        <source>Open Containing Folder</source>
        <translation>Ouvrir le dossier contenant</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="917"/>
        <source>View Current Item</source>
        <translation>Afficher l&apos;élément actuel</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="932"/>
        <location filename="../../mainWin.cpp" line="940"/>
        <source>Enter Password</source>
        <translation>Entrer le mot de passe</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="944"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="945"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="949"/>
        <source>Encrypt the file list</source>
        <translation>Crypter la liste des fichiers</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="950"/>
        <source>This will take effect after files/folders are added.</source>
        <translation>Cela prendra effet une fois que les fichiers/dossiers seront ajoutés.</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1021"/>
        <location filename="../../mainWin.cpp" line="1097"/>
        <source>Extract Into Directory</source>
        <translation>Extraire dans le répertoire</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1111"/>
        <source>Some files will be overwritten.
Do you want to continue?
</source>
        <translation>Certains fichiers seront écrasés.
Voulez-vous continuer&#xa0;?
</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1259"/>
        <source>Link To: %1</source>
        <translation>Lien vers&#xa0;: %1</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1606"/>
        <source>A simple Qt archive manager</source>
        <translation>Un simple gestionnaire d&apos;archives Qt</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1607"/>
        <source>based on libarchive, gzip and 7z</source>
        <translation>basé sur libarchive, gzip et 7z</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1608"/>
        <source>Author</source>
        <translation>Auteur</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1609"/>
        <source>aka.</source>
        <translation>alias.</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1610"/>
        <location filename="../../mainWin.cpp" line="1611"/>
        <source>About Arqiver</source>
        <translation>À propos d&apos;Arqiver</translation>
    </message>
    <message>
        <location filename="../../mainWin.cpp" line="1610"/>
        <source>Translators</source>
        <translation>Traducteurs</translation>
    </message>
</context>
</TS>
